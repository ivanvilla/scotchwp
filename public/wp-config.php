<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scotchbox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',(~8W|*-mXSt,c|Q!ZjUbac@HSOm?7_>6*nT0Kb Hn-RqK?tC^X!t=.<_j^b!R$F');
define('SECURE_AUTH_KEY',  'M[mY|@g~ADi~Dpx-uTk,bHhf&M82Y9l~/G0mYn5}j3wgrV2sL]A]4CDWN]AqWTg}');
define('LOGGED_IN_KEY',    'pRStl!b,/I+KQT>c?0vwgVZEyV9/uKeLz$O7n67M-Zo4(O4k)89m6Ab[;w;rxzm ');
define('NONCE_KEY',        '!%X4uxCc`lCu5&M=[,7st-.+{>YD@1e_.+]?ajH;p$#pR.^D))0Sl?fl<Oa =BoU');
define('AUTH_SALT',        'J|`L,N)+I[1+uD(ojmU-iD+#@~!Q#tdak1B[s9AE[/+Mc{&%<SV1LGzdvLsW(~~O');
define('SECURE_AUTH_SALT', 'uWL` >N8L6@D>{T9EI-mkRWf5UTan|zK$HY]R{5@{QqWxQ)ahO#R8&oWe(noucrW');
define('LOGGED_IN_SALT',   '!d!2Ou*k.4jI5B|eglyXEr9wUoj=KW#9bQ>ZAV@rtco&U2gUF6$qGb9Lbu{jD,6:');
define('NONCE_SALT',       'FKe}0W;AZ&fr[&@bSA|$x9H)GzLit`HI*D+(QIDWTKJ3gxM;]2K_lrP;38;QWR|H');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
